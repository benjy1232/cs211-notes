# What are Pointers?
* Datatype that holds address
* "Nicer" way to work with arrays

# & Operator
* int a = 42; int& ref = a; - Creates reference variable to a
* cout << &a - Address of a

# * operator
* Multiplication
* Create a pointer var

```cpp
#include<iostream>
using namespace std;

int x = 99;

void abc(){
}

int main(void){
	int var = 42;
	int& ref = var;

	int* ptr = &var;

	cout << "&var: " << & var << endl; 
	cout << "ptr: " << ptr << endl;

	cout << "&ptr: " << &ptr << endl;
	cout << "&ref: " << &ref << endl;

	*ptr = 99;
	cout << "var: " << ptr << endl;
}
```
