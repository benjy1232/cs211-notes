#include<iostream>
using namespace std;

int x = 99; //Global variable

int main(void){
	int var = 42;
	int& ref = var;

	int* ptr = &var;

	cout << "&var: " << & var << endl; 
	cout << "ptr: " << ptr << endl;

	cout << "&ptr: " << &ptr << endl;
	cout << "&ref: " << &ref << endl;

	*ptr = 99;
	cout << "var: " << var << endl;
}
