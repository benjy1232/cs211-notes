#include<iostream>
using namespace std;

int* func(){
	int var = 1024;
	int* ptr = &var;
	cout << ptr << endl;
	return ptr;
}

int main(void){
	int* m_ptr = func();
	cout << *m_ptr << endl;

	int a = 99;
	int b = 42;
	cout << "Memory address of a and b" << endl;
	cout << &a << endl << &b << endl;
}
