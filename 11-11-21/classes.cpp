#include<iostream>
using namespace std;

struct customer{
	int cust_id;
	double acct_bal;
};

class customer_class{
private: 
	customer cust;
public:
	void print_customer_details(){
		cout << cust.cust_id << endl;
		cout << cust.acct_bal << endl;
	}

	void set(int cust_id, double acct_bal){
		cust.cust_id = cust_id;
		cust.acct_bal = acct_bal;
	}
};

int main(void){
	customer_class c1;
	c1.set(1000,500);

	customer_class c2;
	
	c1.print_customer_details(); //Output would be 1000, 500
	c2.print_customer_details(); //Output would be garbage values
}
