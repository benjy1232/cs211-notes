# Structures & Classes: Day 2
## Structures
Example of structure:
```cpp
struct customer
{
	int cust_id; //member variables
	double acct_bal;
};

int main(void){
	customer c1;
	customer c2;

	c1.cust_id = 8888;
	c1.acct_bal = 100;

	cout << c1.cust_id << ", " << c1.acct_bal;

	cout << c2.cust_id << ", " << c2.acct_bal;

}
```
This code would not work because c2 has not been initialized so it would have been initialized with garbage values.

### Pointers

```cpp
int main(){
	customer arr[500];
	customer c1;

	int * ptr1;

	customer* ptr;

	ptr = &c1; 
}
```

The line ```customer* ptr ``` would point to a memory address that is of type customer.
However if we tried to do
``` customer& ref_c ```
would not work because reference needs to be assigned from a variable.

### Dynamically creating an array
```cpp
int main(void){
	int * ptr = new int[500]; //Creation of dynamic array of type int

	customer * cust_ptr = new customer[500]; //Creation of dynamic array of type customer
}
```
Or in C it would like
```c
#include<stdio.h>
#include<stdlib.h>

struct customer{
	int acct_id;
	double acct_bal;
};

int main(void){
	int * ptr = malloc(500 * sizeof(int));

	customer * cust_ptr = malloc(500 * sizeof(customer));
}
```
## Classes
Creation of a class:
```cpp
class customer_class{
public:
	int cust_id;
	double acct_bal;

	void print_customer_details(){
		cout << cust_id << endl;
		cout << acct_bal << endl;
	}
};

int main(void){
	customer_class c1;
	c1.cust_id = 1000;
	c1.acct_bal = 500;

	customer_class c2;
	
	c1.print_customer_details(); //Output would be 1000, 500
	c2.print_customer_details(); //Output would be garbage values
}
```
It could also be possible to use a structure within a class so we could do something like this:
```cpp
struct customer{
	int cust_id;
	double acct_bal;
};

class customer_class{
	public:
	customer cust;

	void print_customer_details(){
		cout << cust.cust_id << endl;
		cout << cust.acct_bal << endl;
	}
};

int main(void){
	customer_class c1;
	c1.cust.cust_id = 1000;
	c1.cust.acct_bal = 500;

	customer_class c2;
}