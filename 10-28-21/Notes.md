Code to print out arrays:
```cpp
#include<iostream>
using namespace std;

int main(void){
	int arr[5] = {10,20,30,40,50};

	int * ptr = arr; //&arr[0]

	for(int i=0; i<5; i++){
		cout << ptr[i] << ", ";
	}
}
```
What will happen with this code:
```cpp
#include<iostream>
using namespace std;

int main(void){
	int arr[5] = {10,20,30,40,50};

	int var = 42;
	int &ref = var;

	int * ptr = arr; //&arr[0]
	
	cout << &ptr;

	for(int i=0; i<5; i++){
		cout << ptr[i] << ", ";
	}
}
```
it will print out the address of ptr

What about this one:
```cpp
#include<iostream>
using namespace std;

int * createarray(){
	int arr[5] = {10,20,30,40,50};
	return arr;
}

int main(void){
	int var = 42;
	int &ref = var;

	int * ptr = createarray(); 
	
	cout << &ptr;

	for(int i=0; i<5; i++){
		cout << ptr[i] << ", ";
	}
}
```
it will print out garbage values and the address of what was once arr- in gcc it causes a segmentation fault

Now this code?
```cpp
#include<iostream>
using namespace std;

int * createarray(){
	//int arr[5]={10,20,30,40,50};
	int * arr = new int[5];
	for(int i=0; i<5; i++){
		arr[i]=(i + 1) * 10;
	}
	return arr;
}

int main(void){
	int var = 42;
	int &ref = var;

	int * ptr = createarray(); 
	
	cout << &ptr << endl;

	for(int i=0; i<5; i++){
		cout << ptr[i] << ", ";
	}
}
```
it will print out the address of arr and the values that were stored in arr because arr was stored in heap memory

Heap memory will not delete anything unless asked to, unlike stack memory
