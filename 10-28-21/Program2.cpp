#include<iostream>
using namespace std;

int * createarray(){
	//int arr[5]={10,20,30,40,50};
	int * arr = new int[5];
	for(int i=0; i<5; i++){
		arr[i]=(i + 1) * 10;
	}
	return arr;
}

int main(void){
	int var = 42;
	int &ref = var;

	int * ptr = createarray(); 
	
	cout << &ptr << endl;

	for(int i=0; i<5; i++){
		cout << ptr[i] << ", ";
	}
}
