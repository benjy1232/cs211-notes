#include<iostream>
using namespace std;
//Write a function that searches ar array for an element
//Found-->index
//Not Found --> -1
int recursiveSearch(int arr[], int start_index, int end_index, int searchVal);

int main(void){
    int arr[10]={5,2,8,9,3,23,1,5,9,10};
    cout << recursiveSearch(arr,0,9,10);
    return 0;
}


int recursiveSearch(int arr[], int start_index, int end_index, int searchVal){
    if(arr[start_index] == searchVal){
        return start_index;
    }

    if(start_index+1>end_index){
        return -1;
    }

    int ret = recursiveSearch(arr, start_index + 1, end_index, searchVal);
    return ret;
}