# Structures
Without the use of structures the code for BoA.cpp would have something look like this.
```cpp
int main(){
    char cust_name[5][100];
    double acct_bal[5];
    double loan_due[5];
}
```
With the use of a structure it could look like this
```cpp
struct customer{
    char name[100];
    double acct_bal;
    double loan_due;
};

int main(){
    struct customer customers[5];
}
```
Both of these create arrays that have elements cust_name, acct_bal, and loan_due.
Just one creates them in several different arrays while the other creates it in a single array of type ```customer```.
So in order to access an element from array c1, we would do something like this
```cpp
int main(){
    struct customer customers[5];
    strcpy(customers[0].name, "Serrano");  //Use strcpy here because the array has already been declared
    customers[0].acct_bal = 1000;
    customers[0].loan_due = 0;
}
```
Rest of this program is in structures-example.cpp