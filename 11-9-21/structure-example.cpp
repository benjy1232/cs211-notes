#include<iostream>
#include<string.h>
using namespace std;

struct customer{
    char name[100];
    double acct_bal;
    double loan_due;
};

void set(customer customers[], int num ){
    strcpy(customers[0].name, "Serrano");
    strcpy(customers[1].name, "Tran");
    strcpy(customers[2].name, "Evans");
    strcpy(customers[3].name, "Higares");
    strcpy(customers[4].name, "Montoya");
    for(int i = 0; i<num; i++){
        customers[i].acct_bal = 1000;
        customers[i].loan_due = 0;
    }
    return;
}

void print(customer customers[], int num){
    for(int i=0; i<num; i++){
        cout << customers[i].name << ", ";
        cout << customers[i].acct_bal << ", ";
        cout << customers[i].loan_due << endl;
    }
    return;
}

int main(void){
    customer customers[5];
    set(customers, 5);
    print(customers, 5);
}
