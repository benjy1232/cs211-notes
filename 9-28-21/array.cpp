#include<iostream>
using namespace std;

void printArray(int arr[], int startIndex, int endIndex){
	if(startIndex>endIndex){
		return;
	}
	cout << arr[startIndex] << " ";
	printArray(arr, startIndex+1, endIndex);
	return;
}

int main(void){
	int arr[6] = {1,2,3,4,5,6};

	printArray(arr,0,5);
	cout << endl;
	return 0;
}
