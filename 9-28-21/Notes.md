Write a function that can print out an array

```cpp
int main(void){
	int arr[6] = {1,2,3,4,5,6};

	printArray(arr,0,5);
	cout << endl;
	return 0;
}

void printArray(arr, startIndex, endIndex){
	if(startIndex>endIndex){
		return;
	}
	cout << arr[startIndex] << " ";
	printArray(arr, startIndex+1, endIndex);
	return;
}
```
