#include<iostream>
using namespace std;

int sumArray(int arr[], int &sum, int startIndex, int endIndex);

int main(void){
	int sum=0;
	int array[6]={1,2,3,4,5,6};
	sum=sumArray(array, sum, 0, 5);
	cout << sum << endl;
}

int sumArray(int arr[], int &sum, int startIndex, int endIndex){
	if(startIndex>endIndex){
		return sum;
	}
	sum=sum+arr[startIndex];
	sumArray(arr, sum, startIndex+1, endIndex);
	return sum;
}
