More pointer stuff
```cpp
#include<iostream>
using namespace std;

void copy_arr(int src[], int size, int* dst){
  //Create a new array

  //copy over the elements from src to new array

  //Set dst pointer to new array
}

int main(void){
  int arr[5] = {10,20,30,40,50};
  int* new_arr = NULL;

  copy_arr(arr, 5, new_arr);

  for(int i=0; i<5; i++){
    cout << new_arr[i] << ", ";
  }

  return 0;

}
```
The previous problem is impossible to do because dst is not a reference variable in the function copy_arr.
Changing the code to be more like:
```cpp
#include<iostream>
using namespace std;

void copy_arr(int src[], int size, int*& dst){
  //Create a new array
  int * newArr = new int[size];
  //copy over the elements from src to new array
  for(int i=0; i<size; i++){
    newArr[i] = src[i];
  }
  //Set dst pointer to new array
  dst =  newArr;
}

int main(void){
  int arr[5] = {10,20,30,40,50};
  int* new_arr = nullptr;

  copy_arr(arr, 5, new_arr);

  for(int i=0; i<5; i++){
    cout << new_arr[i] << ", ";
  }

  return 0;

}
```
will make the code work and no longer have any bugs within it

New Problem
------------
```cpp
void swap1(int &x, int &y){
  int temp = x;
  x = y;
  y = temp;
  return;
}

void swap2(int *x, int *y){
  int temp = x;
  x = y;
  y = temp;
}

int main(void){
  int a = 42;
  int b = 99;

  int *ptr1 = &a;
  int *ptr2 = &b;

  swap2(ptr1, ptr2);

  cout << a << ":" << b;
  return 0 ;
}
```
