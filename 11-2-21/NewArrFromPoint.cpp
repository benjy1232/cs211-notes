#include<iostream>
using namespace std;

void copy_arr(int src[], int size, int*& dst){
  //Create a new array
  int * newArr = new int[size];
  //copy over the elements from src to new array
  for(int i=0; i<size; i++){
    newArr[i] = src[i];
  }
  //Set dst pointer to new array
  dst =  newArr;
}

int main(void){
  int arr[5] = {10,20,30,40,50};
  int* new_arr = nullptr;

  copy_arr(arr, 5, new_arr);

  for(int i=0; i<5; i++){
    cout << new_arr[i] << ", ";
  }

  return 0;

}
