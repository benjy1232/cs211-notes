#include<iostream>
using namespace std;

void fibonacciRecursive(int N, int t1, int t2){
	if(N<=0){
		return;
	}
	cout << t1+t2 << " ",
	fibonacciRecursive(N-1, t2, t1+t2);
}

int main(void){
	int n, t1 = 0, t2 = 1;
	cout << "Enter number of terms: ";
	cin >> n;
	cout << "Fibonacci Series: ";
	cout << t1 << " " << t2 << " ";
	fibonacciRecursive(n-2, t1,t2); //Corrects for the first two numbers in fibonacci
	cout << endl;
}
