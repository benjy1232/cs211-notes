A function that calls itself is recursive

Say we have code

```cpp
void main(void){
	int m1 = 10;
	int m2 = 20;
	char m3 = 'm';

	functionA();

	cout << m1;
	cout << m2;
	cout << m3;
}

void functionA(void){
	int A1 = 100;
	int A2 = 200;
	char A3 = 'a';

	functionB();

	cout << A1;
	cout << A2;
	cout << A3;
}

void functionB(void){
	int B1 = 1000;
	int B2 = 2000;
	char B3 = 'b';

	cout << B1;
	cout << B2;
	cout << B3;
}
```

It will then print B1, B2, B3, A1, A2, A3, m1, m2, m3

Stack - Whatever goes in last will come out first - That's how it knows to go to main after exiting functionB

What would happen if a function calls itself?
=============================================
A function calling itself is recursion
It saves the previous instances of itself in the stack and the latest call will be at the top of the stack

Differences between iteration and recursion
-------------------------------------------
* Iteration is when you are using loops to solve something
* Recursion is when you don't use loops, but you are making a function call itself
* Any problem that can be solved with *recursion* can be solved with *iteration*

Matrix - Find Max 
==================
Recursive 1 - Similar to Iteration
----------------------------------
```cpp
max(n,m) = if n>m
		then n
		else m

maxarr([2,1,4,3]) = max(2, maxarr([1,4,3])) -> max(2,4)
	maxarr([1,4,3]) = max(1, maxarr([4,3])) -> max(1,4)
		maxarr([4,3]) = max(4,3)
```
Recurisive 2 - Using Blocks
---------------------------
```cpp
maxarr([2,1,4,3]) = max(maxarr([2,1]), maxarr([4,3]))
		maxarr([2,1]) = max(2,1) -> 2
		maxarr([4,3]) = max(4,3) -> 4
```

Stacks - Recurisve Calls
========================
Take this code for example
```cpp
void main(void){
	int m1 = 10;
	int m2 = 20;
	char m3 = 'm';

	functionA();

	cout << m1;
	cout << m2;
	cout << m3;
}
```

```cpp
void functionA(void){
	int A1 = 2;
	int A2 = 3;
	char A3 = 'a';

	cout << A1;
	cout << A2;

	functionA();

	cout << A3;
}
```
This second code block would never stop so it would run infinitely
In order to fix it we would need to add something like
```cpp
void functionA(int A2=3){ //Not setting A2 equal to 3 at the beginning of every loop, it just defaults to 3 if no value given
	int A1 = 2;
	//int A2 = 3;
	char A3 = 'a';

	if(A2 <=0)
		return;

	cout << A1;
	cout << A2;

	A2--;
	functionA(A2);
	cout << A2;
	cout << A3;
}
```
So it will then print these values out to console
```cpp
2 //A2=3
3
2 //A2=2
2
2 //A2=1
1
a 0 //A2-- = 0
a 1 //A2-- = 1
a 2 //A2-- = 2
10
20
m
```

NEW PROBLEM
===========
Example 1
---------
```cpp
func(x){
	func(x-1);
	cout << x;
}
```

```cpp
func(5)
	func(4)
		func(3)
			func(2)
				func(1)
				x=1
			x=2
		x=3
	x=4
x=5
```
The x-value will not change for each individual function because it is a local variable

Fibonnacci
==========
Recursive Example
-----------------
```cpp
void fibonacciRecursive(int N, int t1, int t2){
	if(N<=0){
		return;
	}
	cout << t1+t2 << " ",
	fibonacciRecursive(N-1, t2, t1+t2);
}

void main(void){
	int n, t1 = 0, t2 = 1;
	cout << "Enter number of terms: ";
	cin >> n;
	cout << "Fibonacci Series: \n";
	cout << t1 << " " << t2 << " ";
	fibonacciRecurisve(n, t1,t2);
}
```
Iteration Example
-----------------
```cpp
int main(void){
	int n;
	int sum=0;
	int prevFib=1;
	int p2Fib=0;
	cout << "Enter number of terms: ";
	cin >> n;
	for(int i=0; i<n; i++){
		cout << sum << ", ";
		p2Fib = prevFib;
		prevFib = sum;
		sum = prevFib + p2Fib;
	}
	return 0;
}
```
