# Final Day with Pointers
How to delete pointers:
```cpp
void function(){
    while(true){
        int * ptr = new int[100000];
        //New is put into  heap memory
        delete[] ptr;
    }
}
```
The new operator is not able to make matrices directly. Instead it can be done with:
```cpp
int** mat = new int*[5];
mat[0] = new int[10];
//The whole matrix would need to be initialized row by row
```

Function to return a matrix using new
```cpp
int ** get_mat(int rows, int cols){
    //Create a matrix of rows x columns
    //This is code I made as an example:
    int ** mat = int*[rows];
    for(int i = 0; i< rows; i++){
        mat[i] = new int[cols];
    }
    //Initialize elements to 0
    for(int j = 0; j < rows; j++){
        for(int k = 0; k < cols; k++){
            mat[j][k] = 0;
        }
    }
}
```
Using these examples it is also possible to create 3-Dimensional arrays.
Code that could do that would look like: 
```cpp
int layers;
int*** cube = new int**[layers];
for(int i=0; i<layers; i++){
    cube[i] = get_mat(rows, cols);
}
```
where it calls upon the previous ```get_mat``` function